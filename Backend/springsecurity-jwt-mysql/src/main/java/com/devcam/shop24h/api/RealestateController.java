package com.devcam.shop24h.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchAutoConfiguration;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.Realestate;
import com.devcam.shop24h.repository.RealestateRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class RealestateController {
    @Autowired
    RealestateRepository realestedRepository;

    @GetMapping("/realestate/{numberPage}")
    public ResponseEntity<List<Realestate>> getAllRealestate(@PathVariable("numberPage") int numberPage) {
        int Length = 9;
        int start = numberPage - 1;
        try {
            List<Realestate> listRealestate = realestedRepository.findRealestate(PageRequest.of(start, Length));
            if (!listRealestate.isEmpty()) {
                return new ResponseEntity<>(listRealestate, HttpStatus.OK);

            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestate")
    public ResponseEntity<List<Realestate>> getRealestate() {
        try {
            List<Realestate> listRealestate = realestedRepository.findAll();
            if (!listRealestate.isEmpty()) {
                return new ResponseEntity<>(listRealestate, HttpStatus.OK);

            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestate/detail/{id}")
    public ResponseEntity<List<Realestate>> getRealestateById(@PathVariable int id) {
        try {
            List<Realestate> RealestateData = realestedRepository.findRealestateByRealeastedId(id);
            if (!RealestateData.isEmpty()) {
                return new ResponseEntity<>(RealestateData, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("project/realestate/{id}/{numberPage}")
    public ResponseEntity<List<Realestate>> getAllRealestateByProjectId(@PathVariable("id") int id,
            @PathVariable("numberPage") int numberPage) {
        int Length = 6;
        int start = numberPage - 1;
        try {
            List<Realestate> listRealestate = realestedRepository.findRealestateByProjectId(id,
                    PageRequest.of(start, Length));
            if (!listRealestate.isEmpty()) {
                return new ResponseEntity<>(listRealestate, HttpStatus.OK);

            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
