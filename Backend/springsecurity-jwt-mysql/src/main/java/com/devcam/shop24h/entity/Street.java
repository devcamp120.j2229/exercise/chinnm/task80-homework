package com.devcam.shop24h.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "street")
public class Street {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "_name")
    private String name;
    @Column(name = "_prefix")
    private String prefix;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "street")
    private List<Project> project;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "street")
    private List<Realestate> realestates;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "_province_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private Province province;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "_district_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private District district;

}
