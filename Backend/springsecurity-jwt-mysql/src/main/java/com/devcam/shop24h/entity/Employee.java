package com.devcam.shop24h.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "employees")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int EmployeeID;
    @Column(name = "LastName")
    private String lastName;
    @Column(name = "FirstName")
    private String firstName;
    @Column(name = "Title")
    private String title;
    @Column(name = "TitleOfCourtesy")
    private String titleOfCourtesy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "BirthDate")
    private Date birthday;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "HireDate")
    private Date hireday;
    @Column(name = "Address")
    private String address;
    @Column(name = "City")
    private String city;
    @Column(name = "Region")
    private String region;
    @Column(name = "PostalCode")
    private String postalCode;
    @Column(name = "Country")
    private String country;
    @Column(name = "HomePhone")
    private String homePhone;
    @Column(name = "Extension")
    private String extension;
    @Column(name = "Photo")
    private String photo;
    @Column(name = "Notes")
    private String note;
    @Column(name = "ReportsTo")
    private int reportTo;
    @Column(name = "Username")
    private String username;

    @Column(name = "Password")
    private String password;
    @Column(name = "Email")
    private String email;

    public enum Activated {
        Y, N,

    }

    @Column(columnDefinition = "ENUM('Y', 'N')")
    @Enumerated(EnumType.STRING)
    private Activated Activated;
    @Column(name = "Profile")
    private String profile;
    @Column(name = "UserLevel")
    private int userLevel;

}
