package com.devcam.shop24h.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcam.shop24h.entity.Realestate;

@Repository
public interface RealestateRepository extends JpaRepository<Realestate, Long> {
    @Query(value = "SELECT * FROM realestates", nativeQuery = true)
    List<Realestate> findRealestate(Pageable pageable);

    @Query(value = "SELECT realestates.* FROM realestates INNER JOIN project ON realestates.project_id = project.id WHERE project.id =:id", nativeQuery = true)
    List<Realestate> findRealestateByProjectId(@Param("id") int id, Pageable pageable);

    @Query(value = "SELECT * FROM realestates WHERE realestates.id = :id", nativeQuery = true)
    List<Realestate> findRealestateByRealeastedId(@Param("id") int id);
}
