package com.devcam.shop24h.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "realestates")
public class Realestate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "title")
    private String title;
    @Column(name = "type")
    private int type;
    @Column(name = "request")
    private int request;
    @Column(name = "address")
    private int address;
    @Column(name = "price")
    private int price;
    @Column(name = "price_min")
    private int priceMin;
    @Column(name = "price_time")
    private int priceTime;

    @Column(name = "date_create")
    @CreatedDate
    private Date createdAt;

    @Column(name = "acreage")
    private BigDecimal acreage;
    @Column(name = "direction")
    private int direction;
    @Column(name = "total_floors")
    private int totalFloor;
    @Column(name = "number_floors")
    private int numberFloor;
    @Column(name = "bath")
    private int bath;
    @Column(name = "apart_code")
    private String apartCode;
    @Column(name = "wall_area")
    private BigDecimal wallArea;
    @Column(name = "bedroom")
    private int bedroom;
    @Column(name = "balcony")
    private int balcony;
    @Column(name = "landscape_view")
    private String landscapeView;
    @Column(name = "apart_loca")
    private int apartLoca;
    @Column(name = "apart_type")
    private int apartType;
    @Column(name = "furniture_type")
    private int furnitureType;
    @Column(name = "price_rent")
    private int priceRent;
    @Column(name = "return_rate")
    private double returnRate;
    @Column(name = "legal_doc")
    private int legalDoc;
    @Column(name = "description")
    private String description;
    @Column(name = "width_y")
    private int Width;
    @Column(name = "long_x")
    private int Long;
    @Column(name = "street_house")
    private int streetHouse;
    @Column(name = "FSBO")
    private int fsbo;
    @Column(name = "view_num")
    private int viewNum;

    @Column(name = "create_by")
    @CreatedBy
    private int createdBy;

    @Column(name = "update_by")
    @LastModifiedBy
    private int updatedBy;
    @Column(name = "shape")
    private String shape;
    @Column(name = "distance2facade")
    private int distance2facade;
    @Column(name = "adjacent_facade_num")
    private int adjacentFacadeNum;
    @Column(name = "adjacent_road")
    private String adjacentRoad;
    @Column(name = "alley_min_width")
    private int alleyMinWidth;
    @Column(name = "adjacent_alley_min_width")
    private int adjacentAlleyMinWidth;
    @Column(name = "factor")
    private int factor;
    @Column(name = "structure")
    private String structure;
    @Column(name = "DTSXD")
    private int dtsxd;
    @Column(name = "CLCL")
    private int clcl;
    @Column(name = "CTXD_price")
    private int ctxdPrice;
    @Column(name = "CTXD_value")
    private int ctxdValue;
    @Column(name = "photo")
    private String photo;
    @Column(name = "_lat")
    private double lat;
    @Column(name = "_lng")
    private double lng;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "district_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private District district;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "province_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private Province province;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "project_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private Project project;

    // @ManyToOne(fetch = FetchType.LAZY)
    // @JoinColumn(name = "customer_id", referencedColumnName = "id", nullable =
    // false)
    // @JsonIgnore
    // private Customer customer;

    // @ManyToOne(fetch = FetchType.LAZY)
    // @JoinColumn(name = "investor_id", referencedColumnName = "id", nullable =
    // false)
    // @JsonIgnore
    // private Investor investor;

    // @ManyToOne(fetch = FetchType.LAZY)
    // @JoinColumn(name = "map_id", referencedColumnName = "id", nullable = false)
    // @JsonIgnore
    // private AddressMap map;
    // @ManyToOne(fetch = FetchType.LAZY)
    // @JoinColumn(name = "district_id",referencedColumnName = "id", nullable =
    // false)
    // @JsonIgnore
    // private District district;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "wards_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private Ward ward;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "street_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private Street street;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private Customer customer;

    public Realestate() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getRequest() {
        return request;
    }

    public void setRequest(int request) {
        this.request = request;
    }

    public int getAddress() {
        return address;
    }

    public void setAddress(int address) {
        this.address = address;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(int priceMin) {
        this.priceMin = priceMin;
    }

    public int getPriceTime() {
        return priceTime;
    }

    public void setPriceTime(int priceTime) {
        this.priceTime = priceTime;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int getTotalFloor() {
        return totalFloor;
    }

    public void setTotalFloor(int totalFloor) {
        this.totalFloor = totalFloor;
    }

    public int getNumberFloor() {
        return numberFloor;
    }

    public void setNumberFloor(int numberFloor) {
        this.numberFloor = numberFloor;
    }

    public int getBath() {
        return bath;
    }

    public void setBath(int bath) {
        this.bath = bath;
    }

    public String getApartCode() {
        return apartCode;
    }

    public void setApartCode(String apartCode) {
        this.apartCode = apartCode;
    }

    public BigDecimal getWallArea() {
        return wallArea;
    }

    public void setWallArea(BigDecimal wallArea) {
        this.wallArea = wallArea;
    }

    public int getBedroom() {
        return bedroom;
    }

    public void setBedroom(int bedroom) {
        this.bedroom = bedroom;
    }

    public int getBalcony() {
        return balcony;
    }

    public void setBalcony(int balcony) {
        this.balcony = balcony;
    }

    public String getLandscapeView() {
        return landscapeView;
    }

    public void setLandscapeView(String landscapeView) {
        this.landscapeView = landscapeView;
    }

    public int getApartLoca() {
        return apartLoca;
    }

    public void setApartLoca(int apartLoca) {
        this.apartLoca = apartLoca;
    }

    public int getApartType() {
        return apartType;
    }

    public void setApartType(int apartType) {
        this.apartType = apartType;
    }

    public int getFurnitureType() {
        return furnitureType;
    }

    public void setFurnitureType(int furnitureType) {
        this.furnitureType = furnitureType;
    }

    public int getPriceRent() {
        return priceRent;
    }

    public void setPriceRent(int priceRent) {
        this.priceRent = priceRent;
    }

    public double getReturnRate() {
        return returnRate;
    }

    public void setReturnRate(double returnRate) {
        this.returnRate = returnRate;
    }

    public int getLegalDoc() {
        return legalDoc;
    }

    public void setLegalDoc(int legalDoc) {
        this.legalDoc = legalDoc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getWidth() {
        return Width;
    }

    public void setWidth(int width) {
        Width = width;
    }

    public int getLong() {
        return Long;
    }

    public void setLong(int l) {
        Long = l;
    }

    public int getStreetHouse() {
        return streetHouse;
    }

    public void setStreetHouse(int streetHouse) {
        this.streetHouse = streetHouse;
    }

    public int getFsbo() {
        return fsbo;
    }

    public void setFsbo(int fsbo) {
        this.fsbo = fsbo;
    }

    public int getViewNum() {
        return viewNum;
    }

    public void setViewNum(int viewNum) {
        this.viewNum = viewNum;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public int getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(int updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public int getDistance2facade() {
        return distance2facade;
    }

    public void setDistance2facade(int distance2facade) {
        this.distance2facade = distance2facade;
    }

    public int getAdjacentFacadeNum() {
        return adjacentFacadeNum;
    }

    public void setAdjacentFacadeNum(int adjacentFacadeNum) {
        this.adjacentFacadeNum = adjacentFacadeNum;
    }

    public String getAdjacentRoad() {
        return adjacentRoad;
    }

    public void setAdjacentRoad(String adjacentRoad) {
        this.adjacentRoad = adjacentRoad;
    }

    public int getAlleyMinWidth() {
        return alleyMinWidth;
    }

    public void setAlleyMinWidth(int alleyMinWidth) {
        this.alleyMinWidth = alleyMinWidth;
    }

    public int getAdjacentAlleyMinWidth() {
        return adjacentAlleyMinWidth;
    }

    public void setAdjacentAlleyMinWidth(int adjacentAlleyMinWidth) {
        this.adjacentAlleyMinWidth = adjacentAlleyMinWidth;
    }

    public int getFactor() {
        return factor;
    }

    public void setFactor(int factor) {
        this.factor = factor;
    }

    public String getStructure() {
        return structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public int getDtsxd() {
        return dtsxd;
    }

    public void setDtsxd(int dtsxd) {
        this.dtsxd = dtsxd;
    }

    public int getClcl() {
        return clcl;
    }

    public void setClcl(int clcl) {
        this.clcl = clcl;
    }

    public int getCtxdPrice() {
        return ctxdPrice;
    }

    public void setCtxdPrice(int ctxdPrice) {
        this.ctxdPrice = ctxdPrice;
    }

    public int getCtxdValue() {
        return ctxdValue;
    }

    public void setCtxdValue(int ctxdValue) {
        this.ctxdValue = ctxdValue;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Ward getWard() {
        return ward;
    }

    public void setWard(Ward ward) {
        this.ward = ward;
    }

    public Street getStreet() {
        return street;
    }

    public void setStreet(Street street) {
        this.street = street;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

}
