package com.devcam.shop24h.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.AddressMap;
import com.devcam.shop24h.repository.AddressMapRepository;

@RestController

public class AddressMapController {
    @Autowired
    AddressMapRepository addressMapRepository;

    @GetMapping("/addressmap")
    public ResponseEntity<List<AddressMap>> getAddress() {
        try {
            List<AddressMap> listRealestate = addressMapRepository.findAll();
            if (!listRealestate.isEmpty()) {
                return new ResponseEntity<>(listRealestate, HttpStatus.OK);

            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
