package com.devcam.shop24h.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcam.shop24h.entity.Utilities;

@Repository
public interface UtilitiesRepository extends JpaRepository<Utilities, Long> {
    // select uttilie by project id
    @Query(value = "SELECT utilities.* from projects_ultilities INNER JOIN project ON project.id = projects_ultilities.project_id INNER JOIN utilities ON projects_ultilities.ultility_id = utilities.id WHERE project.id =:id", nativeQuery = true)
    List<Utilities> findUtilitiesByProjectId(@Param("id") int id);
}
