package com.devcam.shop24h.entity;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "project")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "_name")
    private String name;
    @Column(name = "address")
    private String address;
    @Column(name = "slogan")
    private String slogan;
    @Column(name = "description")
    private String description;
    @Column(name = "acreage")
    private BigDecimal acreage;
    @Column(name = "construct_area")
    private BigDecimal constructArea;
    @Column(name = "num_block")
    private int numBlock;
    @Column(name = "num_floors")
    private String numFloor;
    @Column(name = "num_apartment")
    private int numApartment;
    @Column(name = "apartmentt_area")
    private String apartmentArea;
    @Column(name = "utilities")
    private String utilities;
    @Column(name = "region_link")
    private String regionLink;
    @Column(name = "photo")
    private String photo;
    @Column(name = "_lat")
    private double lat;
    @Column(name = "_lng")
    private double lng;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "_province_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private Province province;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "_district_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private District district;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "_ward_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private Ward ward;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "_street_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private Street street;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "investor", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private Investor investor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "construction_contractor", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private ConstructorContractor constructionContractor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "design_unit", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private DesignUnit designUnit;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "project")
    private List<MasterLayout> masterLayout;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "project")

    private List<Realestate> realestates;

    @ManyToMany
    @JoinTable(name = "projects_ultilities", joinColumns = @JoinColumn(name = "project_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "ultility_id", referencedColumnName = "id"))
    private Collection<Utilities> utilities2;

    @ManyToMany
    @JoinTable(name = "projects_regionlinks", joinColumns = @JoinColumn(name = "project_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "regionlink_id", referencedColumnName = "id"))
    private Collection<RegionLink> regionLink2;

    public Project() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public BigDecimal getConstructArea() {
        return constructArea;
    }

    public void setConstructArea(BigDecimal constructArea) {
        this.constructArea = constructArea;
    }

    public int getNumBlock() {
        return numBlock;
    }

    public void setNumBlock(int numBlock) {
        this.numBlock = numBlock;
    }

    public String getNumFloor() {
        return numFloor;
    }

    public void setNumFloor(String numFloor) {
        this.numFloor = numFloor;
    }

    public int getNumApartment() {
        return numApartment;
    }

    public void setNumApartment(int numApartment) {
        this.numApartment = numApartment;
    }

    public String getApartmentArea() {
        return apartmentArea;
    }

    public void setApartmentArea(String apartmentArea) {
        this.apartmentArea = apartmentArea;
    }

    public String getUtilities() {
        return utilities;
    }

    public void setUtilities(String utilities) {
        this.utilities = utilities;
    }

    public String getRegionLink() {
        return regionLink;
    }

    public void setRegionLink(String regionLink) {
        this.regionLink = regionLink;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Ward getWard() {
        return ward;
    }

    public void setWard(Ward ward) {
        this.ward = ward;
    }

    public Street getStreet() {
        return street;
    }

    public void setStreet(Street street) {
        this.street = street;
    }

    public Investor getInvestor() {
        return investor;
    }

    public void setInvestor(Investor investor) {
        this.investor = investor;
    }

    public ConstructorContractor getConstructionContractor() {
        return constructionContractor;
    }

    public void setConstructionContractor(ConstructorContractor constructionContractor) {
        this.constructionContractor = constructionContractor;
    }

    public DesignUnit getDesignUnit() {
        return designUnit;
    }

    public void setDesignUnit(DesignUnit designUnit) {
        this.designUnit = designUnit;
    }

    public List<MasterLayout> getMasterLayout() {
        return masterLayout;
    }

    public void setMasterLayout(List<MasterLayout> masterLayout) {
        this.masterLayout = masterLayout;
    }

    public List<Realestate> getRealestates() {
        return realestates;
    }

    public void setRealestates(List<Realestate> realestates) {
        this.realestates = realestates;
    }

    public Collection<Utilities> getUtilities2() {
        return utilities2;
    }

    public void setUtilities2(Collection<Utilities> utilities2) {
        this.utilities2 = utilities2;
    }

    public Collection<RegionLink> getRegionLink2() {
        return regionLink2;
    }

    public void setRegionLink2(Collection<RegionLink> regionLink2) {
        this.regionLink2 = regionLink2;
    }

}
