package com.devcam.shop24h.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.Project;
import com.devcam.shop24h.repository.ProjectRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class ProjectController {
    @Autowired
    ProjectRepository projectRepository;

    @GetMapping("/project")
    public ResponseEntity<List<Project>> getProject() {
        try {
            List<Project> listRealestate = projectRepository.findAll();
            if (!listRealestate.isEmpty()) {
                return new ResponseEntity<>(listRealestate, HttpStatus.OK);

            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // @GetMapping("/project/detail/{id}")
    // public ResponseEntity<Project> getRealestateById(@PathVariable int id) {
    // try {
    // Optional<Project> ProjectData = projectRepository.findProjectByProjectId(id);
    // if (ProjectData.isPresent()) {
    // return new ResponseEntity<>(ProjectData.get(), HttpStatus.OK);
    // } else {
    // return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    // }

    // } catch (Exception e) {
    // return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    // }
    // }
    @GetMapping("/project/detail/{id}")
    public ResponseEntity<Project> getProjectById(@PathVariable int id) {
        try {
            Optional<Project> ProjectData = projectRepository.findProjectById(id);
            if (ProjectData.isPresent()) {
                return new ResponseEntity<>(ProjectData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/project/{numberPage}")
    public ResponseEntity<List<Project>> getAllProject(@PathVariable("numberPage") int numberPage) {
        int Length = 6;
        int start = numberPage - 1;
        try {
            List<Project> listRealestate = projectRepository.findProjects(PageRequest.of(start, Length));
            if (!listRealestate.isEmpty()) {
                return new ResponseEntity<>(listRealestate, HttpStatus.OK);

            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
