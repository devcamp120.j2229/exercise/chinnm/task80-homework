package com.devcam.shop24h.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import com.devcam.shop24h.entity.AddressMap;

@Repository

public interface AddressMapRepository extends JpaRepository<AddressMap, Long> {

}
