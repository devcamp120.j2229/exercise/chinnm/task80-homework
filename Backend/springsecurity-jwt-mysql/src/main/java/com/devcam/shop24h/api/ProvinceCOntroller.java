package com.devcam.shop24h.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.Province;
import com.devcam.shop24h.repository.ProvinceRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class ProvinceCOntroller {
    @Autowired
    ProvinceRepository provinceRepository;

    @GetMapping("/province")
    public ResponseEntity<List<Province>> getProvince() {
        try {
            List<Province> listRealestate = provinceRepository.findAll();
            if (!listRealestate.isEmpty()) {
                return new ResponseEntity<>(listRealestate, HttpStatus.OK);

            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
