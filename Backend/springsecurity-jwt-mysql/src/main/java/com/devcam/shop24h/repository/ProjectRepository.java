package com.devcam.shop24h.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcam.shop24h.entity.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
    // select uttilie by project id
    @Query(value = "SELECT project.* FROM project INNER JOIN realestates ON project.id = realestates.project_id WHERE realestates.project_id = :id LIMIT 0,1", nativeQuery = true)
    Optional<Project> findProjectByProjectId(@Param("id") int id);

    @Query(value = "SELECT * FROM project  WHERE project.id = :id ", nativeQuery = true)
    Optional<Project> findProjectById(@Param("id") int id);

    @Query(value = "SELECT * FROM project", nativeQuery = true)
    List<Project> findProjects(Pageable pageable);
}
