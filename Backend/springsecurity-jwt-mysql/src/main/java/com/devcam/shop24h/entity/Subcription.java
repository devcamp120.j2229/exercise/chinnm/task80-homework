package com.devcam.shop24h.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "subcriptions")
public class Subcription {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "user")
    private String user;
    @Column(name = "endpoint")
    private String endpoint;
    @Column(name = "publickey")
    private String publickey;
    @Column(name = "authenticationtoken")
    private String authenticationtoken;
    @Column(name = "contentencoding")
    private String contentencoding;

}
